## Jets in Colour

## Description
This project contains the code to extract the jet-pull angle, dipolarity, jipolarity and other substructure variables from the simulation files found in **'/eos/user/v/vvecchio/JetsInColour/'** on the LXPLUS machines. In the training folder the code for training binary classification neural networks is included. There are 2 networks:
* A network that can be trained to distinguish between ghh and qcd events with 2 subjets
* A network that can be trained to distinguish between ztt and qcd events with 3 subjets

## Setup
1. Run: `python -i dip_jip_jpa_substructure_info_b-tagging_ztt_qcd_all_jpas_pt_range.py [sim_type] [cluster_type] [lower_pt_bound] [upper_pt_bound]` for extracting the variables from the data for ztt ([sim_type] -> ztt) and qcd events with 3 subjets ([sim_type] -> qcd). The p<sub>T</sub> bounds should be in MeV, or use "inf" for infinity as the upper bound.
2. Run `python -i dip_jip_jpa_substructure_info_b-tagging.py [sim_type] [cluster_type]` for ghh ([sim_type] -> ghh) and qcd events with 2 & 3 subjets ([sim_type] -> qcd). You will need to filter out the qcd events with 3 subjets seperately.

    _Note: we found that VR works best for discriminating between colour flow scenarios. Some things might need to be changed in further steps to make it work with VRGhostTag._

    You can also edit **dip_jip_jpa_substructure_info_b-tagging_ztt_qcd_all_jpas_pt_range.py** to work with ghh and qcd events with 2 subjets. This could be useful for investigating the colour flow variables at different p<sub>T</sub>. We didn't have time to do properly do this.

3. To reweight the events so that the ztt & qcd (3 subjets) and also ghh & qcd (2 subjets) events have similar p<sub>T</sub> distributions change the value of `ztt_file` and `qcd_file` accordingly in **training/data-prep/reweighting.py** to the csv files that are outputted in step 1 & 2 (For the 2 subjet case change the value of `ztt_file` to the location of the ghh csv file).
This will output more csv files with the word _reweighted_ in the name, use these in the next steps

4. Run the **data_prep_*.py** scripts in the __training/data-prep__ folder with the right values for `ztt`, `ghh`, and `qcd` in the respective scripts. This will create the csv files for the neural network training.

5. Run **send_fatjets_training_slice.py** in the **training** folder. This is the neural network training and testing script. A more extensive explanation of how to run it will be added in the coming days. For now have a look through the file and look at the PyTorch documentation [here](https://pytorch.org/docs/stable/index.html).
