import pandas as pd
import numpy as np
import h5py
import os
import math
import dask.array as da
from scipy.optimize import minimize

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

import sys # https://www.pythonforbeginners.com/system/python-sys-argv

file_path = '/eos/user/v/vvecchio/JetsInColour/'
print(sys.argv[1])
print(sys.argv[2])
sim_type = str(sys.argv[1]) # Options: "ztt", "ghh", "qcd"
cluster_type = "subjet_"+str(sys.argv[2])+"_" # Options: "VR", "VRGhostTag"

is_hbb = False # is_hbb = True Only use events with 2 subjets in fat jet
is_ztt = False # is_ztt = True Need to calculate dipolarity over the non-b subjets
max_plots = 0 # Per chunk (10000 events)

# Set file IDs based on the event type
if(sim_type == "ztt"):
    is_ztt = True
    is_hbb = False
    file_id1 = 3013
    file_id2 = 3013
    file_id3 = 3013
elif(sim_type == "ghh"):
    is_ztt = False
    is_hbb = True
    file_id1 = 3014
    file_id2 = 3015
    file_id3 = 3057
elif(sim_type == "qcd"):
    is_ztt = False
    is_hbb = False
    file_id1 = 3610
    file_id2 = 3610
    file_id3 = 3610
else:
    raise Exception("Error unknown event type")

def phi_correction(phi):
    if phi > 0.:
        while phi > np.pi:
            phi += np.pi * -2.
    elif phi < 0.:
        while phi < -1. * np.pi:
            phi += np.pi * 2.
    return phi


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def dot(v, w):
    x, y = v
    X, Y = w
    return x * X + y * Y


def length(v):
    x, y = v
    return math.sqrt(x * x + y * y)


def vector(b, e):
    x, y = b
    X, Y = e
    return (X - x, Y - y)


def unit(v):
    x, y = v
    mag = length(v)
    return (x / mag, y / mag)


def distance(p0, p1):
    return length(vector(p0, p1))


def scale(v, sc):
    x, y = v
    return (x * sc, y * sc)


def add(v, w):
    x, y = v
    X, Y = w
    return (x + X, y + Y)


def pnt2line(pnt, start, end):
    # https://stackoverflow.com/a/51240898
    line_vec = vector(start, end)
    pnt_vec = vector(start, pnt)
    line_len = length(line_vec)
    line_unitvec = unit(line_vec)
    pnt_vec_scaled = scale(pnt_vec, 1.0 / line_len)
    t = dot(line_unitvec, pnt_vec_scaled)
    if t < 0.0:
        t = 0.0
    elif t > 1.0:
        t = 1.0
    nearest = scale(line_vec, t)
    dist = distance(nearest, pnt_vec)
    return (dist)

def dipolarity(jetaxes, pTjet, pTcon, coords):
    eta0, phi0, eta1, phi1 = jetaxes
    jet0 = np.array([eta0, phi0])
    jet1 = np.array([eta1, phi1])
    d = np.empty((len(coords)))

    for i in range(len(d)):
        d[i] = pnt2line(coords.iloc[i], jet0, jet1)

    jcm = np.sqrt((jet1[0] - jet0[0]) ** 2 + (phi_correction(jet1[1] - jet0[1])) ** 2)
    dip = np.sum((pTcon/pTjet) * d**2) / (jcm**2)

    return dip

def angle_between(jpv,jcv):
    # Calculate angle between jet-pull vector (jpv) and jet-connection vector (jcv)
    dot = jpv[0]*jcv[0] + jpv[1]*jcv[1]
    det = jpv[0]*jcv[1] - jpv[1]*jcv[0]
    return np.arctan2(det, dot)

suffix_string = cluster_type[6:-1] + "_atan2_h5_"+sim_type+"_b-tagging.csv"
four_momentum_file = "./"+sim_type+"/"+"all_values" + suffix_string # Output file with useful variables

# Write header for the output file with subtructure variables and other useful variables tha may or may not be used for the neural network
with open(four_momentum_file, 'w') as out_var_f:
    out_var_f.write(
        "entry,Split12,Split23,Qw,PlanarFlow,Angularity,Aplanarity,ZCut12,KtDR,Xbb2020v3_QCD,Xbb2020v3_Higgs,Xbb2020v3_Top,JSSTopScore,nVRSubjets,fat_jet_pt,fat_jet_eta,fat_jet_phi,fat_jet_energy,fat_jet_deta,fat_jet_dphi,fat_jet_dr,GhostHBosonsCount,GhostWBosonsCount,GhostZBosonsCount,GhostTQuarksFinalCount,GhostBHadronsFinalCount,GhostCHadronsFinalCount,eventNumber,runNumber,averageInteractionsPerCrossing,actualInteractionsPerCrossing,mass,C2,D2,e3,Tau21_wta,Tau32_wta,FoxWolfram20,"+
        "pt_1,eta_1,phi_1,HadronConeExclTruthLabelID_1,MV2c10_discriminant_1,DL1r_pu_1,DL1r_pc_1,DL1r_pb_1,relativeDeltaRToVRJet_1,HadronConeExclExtendedTruthLabelID_1,numConstituents_1,energy_1,"+
        "pt_2,eta_2,phi_2,HadronConeExclTruthLabelID_2,MV2c10_discriminant_2,DL1r_pu_2,DL1r_pc_2,DL1r_pb_2,relativeDeltaRToVRJet_2,HadronConeExclExtendedTruthLabelID_2,numConstituents_2,energy_2,"+
        "pt_3,eta_3,phi_3,HadronConeExclTruthLabelID_3,MV2c10_discriminant_3,DL1r_pu_3,DL1r_pc_3,DL1r_pb_3,relativeDeltaRToVRJet_3,HadronConeExclExtendedTruthLabelID_3,numConstituents_3,energy_3,"+
        "dipolarity,minimised_dipolarity,jipolarity,b_jipolarity,jetpull_angle,b_jetpull_angle,event_weight")

events = -1
print(sim_type)
print(file_id1)
print(file_id2)
print(file_id3)
for item in os.listdir(file_path):
    #print(file_path + item)
    if (os.path.isdir(file_path + item)):
        if ("user.vvecchio."+str(file_id1) in file_path + item) or ("user.vvecchio."+str(file_id2) in file_path + item) or ("user.vvecchio."+str(file_id3) in file_path + item):
            print(item)
            for filename in os.listdir(file_path + item):
                print(filename)
                if (filename.endswith(".h5")):
                    with open(four_momentum_file,'a') as out_var_f:
                        # Write name of h5 file in output file 
                        out_var_f.write("\n#"+file_path+item+"/"+filename)
                    # Open h5 file
                    with h5py.File(file_path+item+"/"+filename, 'r') as f:
                        # Set up chunks
                        num_of_events_in_file = len(f['fat_jet'])
                        chunk = 0
                        chunk_size = 10000
                        max_chunks = int(np.ceil(float(num_of_events_in_file) / float(chunk_size)))

                        while(chunk != max_chunks):
                            lower_lim = chunk*chunk_size
                            upper_lim = (chunk+1)*chunk_size
                            if upper_lim > num_of_events_in_file:
                                upper_lim = num_of_events_in_file

                            # Lazily loads the data into Dask arrays (https://docs.dask.org/en/stable/generated/dask.array.from_array.html)
                            # Note: .compute() actually loads the necessary data
                            # Without Dask and the chunks it runs incredibly slow becasuse of the large file sizes
                            # Could probably still be implemented better, but this works too
                            event_weights = da.from_array(f['fat_jet'][lower_lim:upper_lim]['mcEventWeight'])
                            substructure_variables = da.from_array(f['fat_jet'][lower_lim:upper_lim][['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','Xbb2020v3_QCD','Xbb2020v3_Higgs','Xbb2020v3_Top','JSSTopScore','nVRSubjets','pt','eta','phi','energy','deta','dphi','dr','GhostHBosonsCount','GhostWBosonsCount','GhostZBosonsCount','GhostTQuarksFinalCount','GhostBHadronsFinalCount','GhostCHadronsFinalCount','eventNumber','runNumber','averageInteractionsPerCrossing','actualInteractionsPerCrossing','mass','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20']])
                            all_fatjet_data = da.from_array(f['fat_jet'][lower_lim:upper_lim][
                                                              ['pt', 'eta', 'phi']])
                            subjet_data_1 = da.from_array(f[cluster_type + '1'][lower_lim:upper_lim][
                                                              ['pt', 'eta', 'phi', 'HadronConeExclTruthLabelID','MV2c10_discriminant', 'DL1r_pu', 'DL1r_pc', 'DL1r_pb', 'relativeDeltaRToVRJet', 'HadronConeExclExtendedTruthLabelID','numConstituents','energy']])
                            subjet_data_2 = da.from_array(f[cluster_type + '2'][lower_lim:upper_lim][
                                                              ['pt', 'eta', 'phi', 'HadronConeExclTruthLabelID','MV2c10_discriminant', 'DL1r_pu', 'DL1r_pc', 'DL1r_pb', 'relativeDeltaRToVRJet', 'HadronConeExclExtendedTruthLabelID','numConstituents','energy']])
                            subjet_data_3 = da.from_array(f[cluster_type + '3'][lower_lim:upper_lim][
                                                              ['pt', 'eta', 'phi', 'HadronConeExclTruthLabelID','MV2c10_discriminant', 'DL1r_pu', 'DL1r_pc', 'DL1r_pb', 'relativeDeltaRToVRJet', 'HadronConeExclExtendedTruthLabelID','numConstituents','energy']])

                            all_tracks_subjet_1 = da.from_array(
                                f[cluster_type + '1_tracks'][lower_lim:upper_lim][['pt', 'eta', 'phi']])
                            all_tracks_subjet_2 = da.from_array(
                                f[cluster_type + '2_tracks'][lower_lim:upper_lim][['pt', 'eta', 'phi']])
                            all_tracks_subjet_3 = da.from_array(
                                f[cluster_type + '3_tracks'][lower_lim:upper_lim][['pt', 'eta', 'phi']])

                            # For loop over all entries
                            max_entries = chunk_size
                            if (chunk == max_chunks - 1):
                                max_entries = num_of_events_in_file % chunk_size
                            plots = 0
                            for i in range(max_entries):
                                events += 1
                                subjet_axes = np.array([subjet_data_1[i].compute(), subjet_data_2[i].compute(),
                                                        subjet_data_3[i].compute()])
                                subjet_tracks = np.array(
                                    [all_tracks_subjet_1[i].compute(), all_tracks_subjet_2[i].compute(),
                                     all_tracks_subjet_3[i].compute()])
                                print("Entry: " + str(i))
                                print("Fetching Subjets")

                                subjet_3_exists = False
                                if(pd.isna(subjet_data_3[i]['pt'].compute()) == False):
                                    subjet_3_exists = True
                                    if(is_hbb):
                                        print("Third subjet exists")
                                        continue

                                main_index = 0
                                secondary_index = 1
                                tertiary_index = 2
                                if (is_ztt):
                                    if (pd.isna(subjet_data_3[i]['pt'].compute())):
                                        print("Not enough subjets")
                                        continue
                                        # Find which subjet is the b-jet and define indices to use later
                                        # DL1r_pb = probability of b-jet determined by DL1r tagger 
                                    if (subjet_axes[2]['DL1r_pb'] >= 0.75 and subjet_axes[0][
                                        'DL1r_pb'] < 0.5 and subjet_axes[1][
                                        'DL1r_pb'] < 0.5):
                                        main_index = 0
                                        secondary_index = 1
                                        tertiary_index = 2
                                    elif (subjet_axes[1]['DL1r_pb'] >= 0.75 and subjet_axes[0][
                                        'DL1r_pb'] < 0.5 and subjet_axes[2][
                                              'DL1r_pb'] < 0.5):
                                        main_index = 0
                                        secondary_index = 2
                                        tertiary_index = 1
                                    elif (subjet_axes[0]['DL1r_pb'] >= 0.75 and subjet_axes[1][
                                        'DL1r_pb'] < 0.5 and subjet_axes[2]['DL1r_pb'] < 0.5):
                                        main_index = 1
                                        secondary_index = 2
                                        tertiary_index = 0
                                    else:
                                        print("Subjets do not have the right types")
                                        continue

                                if (pd.isna(subjet_data_2[i]['pt'].compute())):
                                    #print("Not enough subjets")
                                    continue

                                main_subjet = pd.DataFrame(subjet_tracks[main_index]).dropna()
                                main_subjet_pt = subjet_axes[main_index]['pt']
                                secondary_subjet = pd.DataFrame(subjet_tracks[secondary_index]).dropna()
                                secondary_subjet_pt = subjet_axes[secondary_index]['pt']
                                tertiary_subjet = pd.DataFrame(subjet_tracks[tertiary_index]).dropna()
                                jet_axis_0 = subjet_axes[main_index].copy()
                                jet_axis_1 = subjet_axes[secondary_index]
                                jet_axis_2 = subjet_axes[tertiary_index]

                                # Code below transforms the coordinates of the tracks so that jet_axis_0 is at the origin
                                jet_axis_0['eta'] = 0
                                jet_axis_0['phi'] = 0

                                jet_axis_1['eta'] = subjet_axes[secondary_index]['eta']-subjet_axes[main_index]['eta']
                                jet_axis_1['phi'] = phi_correction(subjet_axes[secondary_index][('phi')]-subjet_axes[main_index][('phi')])

                                jet_axis_2['eta'] = subjet_axes[tertiary_index]['eta'] - subjet_axes[main_index]['eta']
                                jet_axis_2['phi'] = phi_correction(subjet_axes[tertiary_index]['phi'] - subjet_axes[main_index]['phi'])

                                tracks_subjet_1 = pd.concat([main_subjet['pt'], main_subjet['eta']-subjet_axes[main_index]['eta'], (main_subjet['phi']-subjet_axes[main_index]['phi']).apply(lambda x: phi_correction(x))], axis=1)
                                tracks_subjet_2 = pd.concat([secondary_subjet['pt'], secondary_subjet['eta']-subjet_axes[main_index]['eta'], (secondary_subjet['phi']-subjet_axes[main_index]['phi']).apply(lambda x: phi_correction(x))], axis=1)
                                tracks_subjet_3 = pd.concat([tertiary_subjet['pt'], tertiary_subjet['eta']-subjet_axes[main_index]['eta'], (tertiary_subjet['phi']-subjet_axes[main_index]['phi']).apply(lambda x: phi_correction(x))], axis=1)

                                fatjet_data = all_fatjet_data[i].compute()
                                fatjet_data['eta'] = fatjet_data['eta'] - subjet_axes[main_index]['eta']
                                fatjet_data['phi'] = phi_correction(fatjet_data['phi'] - subjet_axes[main_index]['phi'])

                                #del subjet_axes
                                del subjet_tracks
                                total_pt = main_subjet_pt + secondary_subjet_pt
                                merged_tracks = pd.concat([tracks_subjet_1, tracks_subjet_2])

                                # Calculate backward and forward jet-pull vector
                                j_p_vec = np.array([0, 0])
                                b_j_p_vec = np.array([0, 0])
                                max_subentries = max([len(main_subjet),len(secondary_subjet)])

                                for subentry in range(max_subentries):
                                    if(subentry < len(main_subjet)):
                                        r_c = np.array([tracks_subjet_1['eta'][subentry] - jet_axis_0['eta'],
                                                        phi_correction(tracks_subjet_1['phi'][subentry] - jet_axis_0['phi'])])
                                        j_p_vec = j_p_vec + (np.linalg.norm(r_c) * tracks_subjet_1['pt'][
                                            subentry] / main_subjet_pt) * r_c
                                    if(subentry < len(secondary_subjet)):
                                        b_r_c = np.array([tracks_subjet_2['eta'][subentry] - jet_axis_1['eta'],
                                                        phi_correction(
                                                            tracks_subjet_2['phi'][subentry] - jet_axis_1['phi'])])
                                        b_j_p_vec = b_j_p_vec + (np.linalg.norm(b_r_c) * tracks_subjet_2['pt'][
                                            subentry] / secondary_subjet_pt) * b_r_c

                                print("Final jet pull vector: " + str(j_p_vec))

                                bnds = ((-2, 2), (-np.pi, +np.pi), (-2, 2), (-np.pi, +np.pi))
                                jetaxes_arr = np.array([jet_axis_0['eta'], jet_axis_0['phi'], jet_axis_1['eta'],jet_axis_1['phi']])
                                #cons = [{'type': 'eq', 'fun': lambda x: np.sqrt(np.square(x[2]-x[0]) + np.square(phi_correction(x[3]-x[1]))) - np.sqrt(np.square(jetaxes_arr[2]-jetaxes_arr[0]) + np.square(phi_correction(jetaxes_arr[3]-jetaxes_arr[1])))}]#,
                                        #{'type': 'eq', 'fun': lambda x: (x[2]+x[0])/2 - (jetaxes_arr[0]+jetaxes_arr[2])/2},
                                        #{'type': 'eq', 'fun': lambda x: (x[1]+x[3])/2 - (jetaxes_arr[1]+jetaxes_arr[3])/2}]
                                try:
                                    # Minimise dipolarity for calculating jipolarity
                                    dip_min = minimize(dipolarity, jetaxes_arr,
                                                                      args=(total_pt, merged_tracks['pt'], merged_tracks[['eta', 'phi']]), bounds=bnds)#, constraints=cons)
                                except ZeroDivisionError as err:
                                    print("Minimization failed: ", err)
                                    continue

                                print(dip_min.success)
                                if(dip_min.success == False):
                                    print("Minimization failed: \n" + str(dip_min.message))
                                    continue

                                print("Minimisation successful")
                                # Calculate Dipolarity
                                dip_value = dipolarity(np.array(
                                    [jet_axis_0['eta'], jet_axis_0['phi'], jet_axis_1['eta'], jet_axis_1['phi']]),
                                                       total_pt, merged_tracks['pt'], merged_tracks[['eta', 'phi']])
                                print("Dipolarity:" + str(dip_value))
                                print("Minimised Dipolarity: " +str(dip_min.fun))
                                print("Old jet axes: " +str(jetaxes_arr))
                                print("New jet axes: " +str(dip_min.x))

                                # Calculate forward and backward jipolarity
                                new_jet_connection_vector = np.array([dip_min.x[2] - dip_min.x[0], dip_min.x[3] - dip_min.x[1]])
                                # Add b_new_jet_connection_vector
                                jipolarity = angle_between(j_p_vec, new_jet_connection_vector)
                                b_jipolarity = angle_between(b_j_p_vec, new_jet_connection_vector)

                                # Calculate forward and backward jet-pull angle
                                jet_connection_vector = np.array([jet_axis_1['eta'] - jet_axis_0['eta'],
                                    phi_correction(jet_axis_1['phi'] - jet_axis_0['phi'])])
                                jetpull_angle = angle_between(j_p_vec, jet_connection_vector)
                                b_jet_connection_vector = np.array([jet_axis_0['eta'] - jet_axis_1['eta'],
                                    phi_correction(jet_axis_0['phi'] - jet_axis_1['phi'])])
                                b_jetpull_angle = angle_between(b_j_p_vec, b_jet_connection_vector)

                                print("Jipolarity :" +str(jipolarity))
                                print("Jet-Pull Angle: " + str(jetpull_angle))

                                # Output jet information
                                with open(four_momentum_file, 'a') as out_var_f:
                                    out_var_f.write("\n" + str(events) + "," +str(substructure_variables[i].compute())[1:-1].replace(" ","") +
                                                    "," + str(subjet_data_1[i].compute())[1:-1].replace(" ","") +
                                                    "," + str(subjet_data_2[i].compute())[1:-1].replace(" ","") +
                                                    "," + str(subjet_data_3[i].compute())[1:-1].replace(" ","") + "," + str(
                                                    dip_value) + "," + str(dip_min.fun) + "," + str(
                                                    jipolarity) + "," + str(b_jipolarity)+"," + str(
                                                    jetpull_angle) + "," + str(b_jetpull_angle) + "," + str(event_weights[i].compute()))


                                if (plots >= max_plots):
                                    continue

                                """
                                PLOTTING CODE
                                """
                                figure, axes = plt.subplots(figsize=(10, 10))
                                plt.xlabel(r"$\eta$", fontsize=15)
                                plt.ylabel(r"$\phi$", fontsize=15)

                                axes.plot([jet_axis_0['eta'],jet_axis_1['eta']],[jet_axis_0['phi'],jet_axis_1['phi']],label="Original jet axis",c='b')
                                axes.plot([dip_min.x[0],dip_min.x[2]],[dip_min.x[1],dip_min.x[3]],label="Jet axis after minimisation",linestyle="--",c='k')
                                axes.annotate("", xy=(j_p_vec[0]*1e3, j_p_vec[1]*1e3), xytext=(0, 0),arrowprops = dict(arrowstyle="->", color='r', label=r"Jet-Pull Vector * $10^3$"))
                                axes.arrow(0,0,j_p_vec[0]*1e3,j_p_vec[1]*1e3,color='r',label=r"Jet-Pull Vector * $10^3$") # Only for displaying arrow in legend?

                                if (subjet_3_exists):
                                    cluster_pt = np.hstack((tracks_subjet_1['pt'], tracks_subjet_2['pt'], tracks_subjet_3['pt']))
                                else:
                                    cluster_pt = np.hstack((tracks_subjet_1['pt'], tracks_subjet_2['pt']))

                                norm = LogNorm(vmin=100 * 1e-3, vmax=max(tracks_subjet_1['pt'] * 1e-3))
                                axes.scatter(tracks_subjet_1['eta'].to_numpy(),
                                             tracks_subjet_1['phi'].to_numpy(),
                                             c=tracks_subjet_1['pt'].to_numpy() * 1e-3, norm=norm, cmap="Blues")
                                axes.scatter(tracks_subjet_2['eta'].to_numpy(),
                                             tracks_subjet_2['phi'].to_numpy(),
                                             c=tracks_subjet_2['pt'].to_numpy() * 1e-3, norm=norm, cmap="Reds")
                                if (subjet_3_exists == True):
                                    axes.scatter(tracks_subjet_3['eta'].to_numpy(),
                                                 tracks_subjet_3['phi'].to_numpy(),
                                                 c=tracks_subjet_3['pt'].to_numpy() * 1e-3, norm=norm, cmap="Greens")

                                # for i in range(len(eta_phi_unique[:,0])):
                                #	colour = 'k'
                                #	if len(jet_axes) > 1:
                                #		if (eta_phi_unique[i, 0] in subjet_0_constituents[:, 2]) and (eta_phi_unique[i, 1] in subjet_0_constituents[:, 3]):
                                #			colour = 'b'
                                #		elif (eta_phi_unique[i, 0] in subjet_1_constituents[:, 2]) and (eta_phi_unique[i, 1] in subjet_1_constituents[:, 3]):
                                #			colour = 'r'
                                #	axes.scatter(eta_phi_unique[i, 0], eta_phi_unique[i, 1], c=colour)

                                props = dict(boxstyle='round', facecolor='white', alpha=0.6)

                                plt.text(jet_axis_0['eta'] + (0.33 / np.sqrt(2)),
                                         jet_axis_0['phi'] + (0.33 / np.sqrt(2)),
                                         s=r"$j_0$", fontsize=15)
                                plt.text(jet_axis_1['eta'] + (0.33 / np.sqrt(2)),
                                         jet_axis_1['phi'] + (0.33 / np.sqrt(2)),
                                         s=r"$j_1$", fontsize=15)
                                if(subjet_3_exists):
                                    plt.text(jet_axis_2['eta'] + (0.33 / np.sqrt(2)),
                                             jet_axis_2['phi'] + (0.33 / np.sqrt(2)),
                                             s=r"$j_2$", fontsize=15)

                                subjet_pt_str = r"Subjet $p_T$:" + "\n" + r"$j_0$ = " + format(jet_axis_0['pt'] * 1e-3,
                                                                  '3.2e') + " GeV\n" + r"$j_1$ = " + format(
                                    jet_axis_1['pt'] * 1e-3, '3.2e') + " GeV" + str("\n"+r"$j_2$ = " + format(
                                    jet_axis_2['pt'] * 1e-3, '3.2e') + " GeV") if subjet_3_exists else r"Subjet $p_T$:" + "\n" + r"$j_0$ = " + format(jet_axis_0['pt'] * 1e-3,
                                                                  '3.2e') + " GeV\n" + r"$j_1$ = " + format(
                                    jet_axis_1['pt'] * 1e-3, '3.2e') + " GeV"


                                # subjet_pt_text = axes.text(0.97,0.02, subjet_pt_str,transform=axes.transAxes, fontsize=10,
                                # verticalalignment='bottom', bbox=props)
                                circles = []
                                axes.set_aspect(1)
                                plt.scatter(fatjet_data['eta'], fatjet_data['phi'], s=2, c='k')
                                l_circle = plt.Circle((fatjet_data['eta'], fatjet_data['phi']), 1.,
                                                      fill=False, color='k')
                                axes.add_patch(l_circle)

                                # for i in range(len(jet_axes['fjet_subjet_pt'])):
                                phi = jet_axis_0['phi']
                                circle = plt.Circle((jet_axis_0['eta'], phi), 0.4, alpha=0.1)
                                # circles.append(circle)
                                axes.add_patch(circle)

                                phi = jet_axis_1['phi']
                                circle = plt.Circle((jet_axis_1['eta'], phi), 0.4, alpha=0.1)
                                # circles.append(circle)
                                axes.add_patch(circle)
                                if(subjet_3_exists):
                                    phi = jet_axis_2['phi']
                                    circle = plt.Circle((jet_axis_2['eta'], phi), 0.4, alpha=0.1)
                                    # circles.append(circle)
                                    axes.add_patch(circle)

                                # place a text box in upper left in axes coords

                                axes.text(0.02, 0.97,
                                          r"Fat Jet $p_T$: " + format(fatjet_data['pt'] * 1e-3,
                                                                      '3.2e') + " GeV\n" + subjet_pt_str,
                                          transform=axes.transAxes, fontsize=12,
                                          verticalalignment='top', bbox=props)

                                axes.text(0.02, 0.02,
                                          r"Dipolarity: " + format(dip_value,'4.3e') + "\nJet-Pull Angle: " +format(jetpull_angle,'4.3f') + "\nJipolarity: "+format(jipolarity,'4.3f'),
                                          transform=axes.transAxes, fontsize=12,
                                          verticalalignment='bottom', bbox=props)
                                fatjet_axis = np.array([fatjet_data['eta'], fatjet_data['phi']])
                                ymin = -3.2 if fatjet_axis[1] - 1. < -np.pi else fatjet_axis[1] - 1.1
                                ymax = 3.2 if fatjet_axis[1] + 1. > np.pi else fatjet_axis[1] + 1.4
                                xmin = 0
                                xmax = 0
                                if ((ymin == -3.2) or (ymax == 3.2)):
                                    xmin = -2.5
                                    xmax = 2.5
                                    ymin = -3.2
                                    ymax = 3.2
                                else:
                                    xmin = fatjet_axis[0] - 1.4
                                    xmax = fatjet_axis[0] + 1.1
                                axes.set_xlim([xmin, xmax])
                                axes.set_ylim([ymin, ymax])
                                axes.tick_params(axis='x', labelsize=12)
                                axes.tick_params(axis='y', labelsize=12)
                                if(is_ztt):
                                    axes.set_title(
                                        r"$Z' \rightarrow t\bar{t}$" + " (" + cluster_type[7:-1] + ") " + str(i),
                                        fontdict={'fontsize': 15})
                                elif(is_hbb):
                                    axes.set_title(
                                        r"$G \rightarrow HH \rightarrow bb\bar{b}\bar{b}$ " + " (" + cluster_type[7:-1] + ") " + str(i),
                                        fontdict={'fontsize': 15})
                                else:
                                    axes.set_title(
                                        r"QCD" + " (" + cluster_type[7:-1] + ") " + str(i),
                                        fontdict={'fontsize': 15})

                                divider = make_axes_locatable(axes)
                                cax1 = divider.append_axes("right", size="5%", pad=0.05)
                                cmap1 = cm.ScalarMappable(norm=norm, cmap='Blues')
                                cmap1.set_array(cluster_pt * 1e-3)
                                cbar1 = figure.colorbar(cmap1, ax=axes, cax=cax1, shrink=0.5)
                                cbar1.ax.set_yticks([])
                                cbar1.ax.set_yticklabels([])

                                cax2 = divider.append_axes("right", size="5%", pad=0.05)
                                cmap2 = cm.ScalarMappable(norm=norm, cmap='Reds')
                                cmap2.set_array(cluster_pt * 1e-3)
                                cbar2 = figure.colorbar(cmap2, ax=axes, cax=cax2, shrink=0.5)
                                cbar2.ax.set_yticks([])
                                cbar2.ax.set_yticklabels([])

                                cax3 = divider.append_axes("right", size="5%", pad=0.05)
                                cmap3 = cm.ScalarMappable(norm=norm, cmap='Greens')
                                cmap3.set_array(cluster_pt * 1e-3)
                                cbar3 = figure.colorbar(cmap3, ax=axes, cax=cax3, shrink=0.5)
                                cbar3.ax.tick_params(labelsize=12)
                                cbar3.ax.set_ylabel(r"Constituent $p_T$ (GeV)", fontsize=15)
                                axes.legend(loc='upper right', fontsize=12)
                                print("saving figure..")
                                plt.savefig("scatterplots/"+sim_type+"/"+cluster_type[7:-1]+"/dip_jpa_jip_bounds_only_{}.png".format(events))
                                plt.close()
                                """
                                END OF PLOTTING CODE
                                """
                                plots += 1
                            chunk += 1
                    continue
                else:
                    continue
        else:
            continue
    else:
        continue

