import pandas as pd

ztt = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas_reweighted.csv", low_memory=False)
qcd = pd.read_csv("all_values_VR_atan2_h5_qcd_b-tagging_all_jpas_reweighted.csv", low_memory=False)

ztt.drop(ztt.index[ztt['GhostTQuarksFinalCount'] == 0], inplace=True)

ztt_full=pd.concat([ztt, qcd]).sample(frac=1).reset_index(drop=True)
all_variables = ztt_full[['fat_jet_pt','fat_jet_eta','fat_jet_phi','fat_jet_energy','Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                         'dipolarity_1_2','dipolarity_1_3','dipolarity_2_3',
                         'jipolarity_1_2','jipolarity_1_3','jipolarity_2_3',
                         'b_jipolarity_1_2','b_jipolarity_1_3','b_jipolarity_2_3',
                         'jetpull_angle_1_2','jetpull_angle_1_3','jetpull_angle_2_3',
                         'b_jetpull_angle_1_2','b_jetpull_angle_1_3','b_jetpull_angle_2_3',
                         'event_weight','GhostTQuarksFinalCount']]
control = ztt_full[['fat_jet_pt','fat_jet_eta','fat_jet_phi','fat_jet_energy','Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                   'event_weight','GhostTQuarksFinalCount']]


slice=[]
slice_control=[]


slice.append(all_variables[ztt_full['fat_jet_pt'] < 300000].sample(n=5620))

slice.append(all_variables[(ztt_full['fat_jet_pt'] > 300000) & (ztt_full['fat_jet_pt'] < 350000)].sample(n=5620))

slice.append(all_variables[(ztt_full['fat_jet_pt'] > 350000) & (ztt_full['fat_jet_pt'] < 400000)].sample(n=5620))

slice.append(all_variables[(ztt_full['fat_jet_pt'] > 400000) & (ztt_full['fat_jet_pt'] < 450000)].sample(n=5620))

slice.append(all_variables[(ztt_full['fat_jet_pt'] > 450000)])



#slice7=all_variables[(ghh_full['fat_jet_pt'] > 600000)]


for i in range(len(slice)):
    print(len(slice[i]))
    slice_control.append(slice[i][['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                       'event_weight','GhostTQuarksFinalCount']])
    slice_control[i].to_csv('ztt_s'+str(i+1)+'_VR_training_control.csv')
    slice[i]=slice[i][['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                         'dipolarity_1_2','dipolarity_1_3','dipolarity_2_3',
                         'jipolarity_1_2','jipolarity_1_3','jipolarity_2_3',
                         'b_jipolarity_1_2','b_jipolarity_1_3','b_jipolarity_2_3',
                         'jetpull_angle_1_2','jetpull_angle_1_3','jetpull_angle_2_3',
                         'b_jetpull_angle_1_2','b_jetpull_angle_1_3','b_jetpull_angle_2_3',
                         'event_weight','GhostTQuarksFinalCount']]
    slice[i].to_csv('ztt_s'+str(i+1)+'_VR_training_all_variables.csv')




all_variables = ztt_full[['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                         'dipolarity_1_2','dipolarity_1_3','dipolarity_2_3',
                         'jipolarity_1_2','jipolarity_1_3','jipolarity_2_3',
                         'b_jipolarity_1_2','b_jipolarity_1_3','b_jipolarity_2_3',
                         'jetpull_angle_1_2','jetpull_angle_1_3','jetpull_angle_2_3',
                         'b_jetpull_angle_1_2','b_jetpull_angle_1_3','b_jetpull_angle_2_3',
                         'event_weight','GhostTQuarksFinalCount']]
control = ztt_full[['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                   'event_weight','GhostTQuarksFinalCount']]
control = control.sample(frac=1).reset_index(drop=True)
all_variables = all_variables.sample(frac=1).reset_index(drop=True)

all_variables.to_csv('ztt_weighted_VR_training_all_variables.csv')
control.to_csv('ztt_weighted_VR_training_control.csv')
